<?php
# include the simple ads class:
require('amazon_simple_ads_class.php');

# set your keys and Associate Tag
$pub = '<your public Access Key>';
$priv = '<your Secret Key>';
$tag = '<yourAssociateTag>'; // e.g. "foobar-21"

# Now instanciate the class (4th parameter is optional and defaults to 'de'):
$ama = new AmazonAds($pub,$priv,$tag,'de');

# optionally, if you wish content in other than the default language:
$ama->setLangPref('en_GB');

# Get details on one or more (comma separated) ASINs:
$res = $ama->getItemByAsin('3527760687,B000E8JNL0,B000G1ECL6');
echo "Results: "; print_r($res);

# Search for products in a given category (aka SearchIndex):
$res = $ama->getItemByKeyword('Android Battery','Electronics');
echo "Results: "; print_r($res);

# Use results:
foreach ($res['items'] as $item) {
  echo "Title: " . $item['title']
   . "\nPrice: " . $item['price']
   . "\nURL  : " . $item['url']
   . "\nImage: " . $item['img']
   . "\n\n";
}

?>