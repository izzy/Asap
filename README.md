## Amazons Product Advertising API for PHP
This is a collection of PHP classes to enable you to access the Amazons Product
Advertising API (PA API 5) easily from within PHP – with the goal of placing some ads on
your site, so they generate you some money to make up for all the hard work you
put into building and maintaining it, whenever a visitor clicks an ad and buys
something at Amazon.

Other than the SDK offered by Amazon (which consists of 221 files summing up to 2.1M),
with *Asap* you only need 3 files summing up to 30K. Integration should be much easier.
The feature set is much smaller, too – but sufficient for most cases.

Further, using local caching and acting like a proxy, *Asap* respects the privacy
of your visitors: there's no user-specific data transmitted to Amazon unless the
user actively follows a link from the ad you've placed.

While this can be used as drop-in replacement for its predecessor for PA API 4 (which
can still be found in the paapi4 branch), note that the CodeDiesel part has been
dropped – and the project now comes under the Apache 2.0 license (instead of GPL 2).

In case you wonder about the name: *AsAP* stands for *Amazon simple Api for PHP.*


### Where is it used?
You might wish to see for yourself what the outcome might be, so some example places
are listed here (if you use *Asap* and want to be mentioned here, just let me know):

* [IzzyOnDroid](https://android.izzysoft.de/)
* [eBooks.Qumran.Org](https://ebooks.qumran.org/)


### Requirements
* Amazon PartnerNet account
* an associate ID you can generate at the PartnerNet site
* a pair of public/private key you can create at the AWS site
* PHP 7.0 or higher
* some coding knowledge: this is an API, i.e. an Application Programmers
  Interface – not a ready-to-use application


### Credits
This is based on the AWSv4 Signing class for PA API 5 [provided by Amazon](https://webservices.amazon.com/paapi5/documentation/without-sdk.html#php)
(in this code: `awsv4.class.php`). To avoid license conflicts (or mishmash), this project hence sticks to their Apache 2.0 license
(other than my other projects, which usually come as GPL-2.0 licensed).


### More details
For installation instructions, usage, and more, please refer to the
[`README.md`](doc/README.md) file located in the `doc/` sub-directory. To get
an idea of how simple integration is, see the [`Example.php`](Example.php)
file, which offers you a simple and commented quick-start.
